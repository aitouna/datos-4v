﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Formulario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnBuscarDNI = New System.Windows.Forms.Button()
        Me.btnBuscarMatri = New System.Windows.Forms.Button()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.lblDNI = New System.Windows.Forms.Label()
        Me.lblMatri = New System.Windows.Forms.Label()
        Me.lblGrado = New System.Windows.Forms.Label()
        Me.lblAño = New System.Windows.Forms.Label()
        Me.btnAñadir = New System.Windows.Forms.Button()
        Me.btnAlumnosCurso = New System.Windows.Forms.Button()
        Me.lblMosNombre = New System.Windows.Forms.Label()
        Me.lblMosDNI = New System.Windows.Forms.Label()
        Me.lblMosMatri = New System.Windows.Forms.Label()
        Me.lblMosGrado = New System.Windows.Forms.Label()
        Me.lblMosAño = New System.Windows.Forms.Label()
        Me.btnBorrar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnBuscarDNI
        '
        Me.btnBuscarDNI.Location = New System.Drawing.Point(49, 30)
        Me.btnBuscarDNI.Name = "btnBuscarDNI"
        Me.btnBuscarDNI.Size = New System.Drawing.Size(194, 27)
        Me.btnBuscarDNI.TabIndex = 0
        Me.btnBuscarDNI.Text = "Buscar alumno por DNI"
        Me.btnBuscarDNI.UseVisualStyleBackColor = True
        '
        'btnBuscarMatri
        '
        Me.btnBuscarMatri.Location = New System.Drawing.Point(250, 29)
        Me.btnBuscarMatri.Name = "btnBuscarMatri"
        Me.btnBuscarMatri.Size = New System.Drawing.Size(194, 27)
        Me.btnBuscarMatri.TabIndex = 1
        Me.btnBuscarMatri.Text = "Buscar alumno por nº matricula"
        Me.btnBuscarMatri.UseVisualStyleBackColor = True
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(49, 73)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(50, 15)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Text = "Nombre:"
        '
        'lblDNI
        '
        Me.lblDNI.AutoSize = True
        Me.lblDNI.Location = New System.Drawing.Point(49, 98)
        Me.lblDNI.Name = "lblDNI"
        Me.lblDNI.Size = New System.Drawing.Size(33, 15)
        Me.lblDNI.TabIndex = 3
        Me.lblDNI.Text = "DNI:"
        '
        'lblMatri
        '
        Me.lblMatri.AutoSize = True
        Me.lblMatri.Location = New System.Drawing.Point(49, 123)
        Me.lblMatri.Name = "lblMatri"
        Me.lblMatri.Size = New System.Drawing.Size(75, 15)
        Me.lblMatri.TabIndex = 4
        Me.lblMatri.Text = "Nº matricula:"
        '
        'lblGrado
        '
        Me.lblGrado.AutoSize = True
        Me.lblGrado.Location = New System.Drawing.Point(49, 149)
        Me.lblGrado.Name = "lblGrado"
        Me.lblGrado.Size = New System.Drawing.Size(41, 15)
        Me.lblGrado.TabIndex = 5
        Me.lblGrado.Text = "Grado:"
        '
        'lblAño
        '
        Me.lblAño.AutoSize = True
        Me.lblAño.Location = New System.Drawing.Point(49, 174)
        Me.lblAño.Name = "lblAño"
        Me.lblAño.Size = New System.Drawing.Size(30, 15)
        Me.lblAño.TabIndex = 6
        Me.lblAño.Text = "Año:"
        '
        'btnAñadir
        '
        Me.btnAñadir.Location = New System.Drawing.Point(52, 238)
        Me.btnAñadir.Name = "btnAñadir"
        Me.btnAñadir.Size = New System.Drawing.Size(190, 27)
        Me.btnAñadir.TabIndex = 7
        Me.btnAñadir.Text = "Añadir nuevo alumno"
        Me.btnAñadir.UseVisualStyleBackColor = True
        '
        'btnAlumnosCurso
        '
        Me.btnAlumnosCurso.Location = New System.Drawing.Point(250, 238)
        Me.btnAlumnosCurso.Name = "btnAlumnosCurso"
        Me.btnAlumnosCurso.Size = New System.Drawing.Size(194, 27)
        Me.btnAlumnosCurso.TabIndex = 9
        Me.btnAlumnosCurso.Text = "Buscar alumnos"
        Me.btnAlumnosCurso.UseVisualStyleBackColor = True
        '
        'lblMosNombre
        '
        Me.lblMosNombre.AutoSize = True
        Me.lblMosNombre.Location = New System.Drawing.Point(155, 73)
        Me.lblMosNombre.Name = "lblMosNombre"
        Me.lblMosNombre.Size = New System.Drawing.Size(0, 15)
        Me.lblMosNombre.TabIndex = 10
        '
        'lblMosDNI
        '
        Me.lblMosDNI.AutoSize = True
        Me.lblMosDNI.Location = New System.Drawing.Point(155, 98)
        Me.lblMosDNI.Name = "lblMosDNI"
        Me.lblMosDNI.Size = New System.Drawing.Size(0, 15)
        Me.lblMosDNI.TabIndex = 11
        '
        'lblMosMatri
        '
        Me.lblMosMatri.AutoSize = True
        Me.lblMosMatri.Location = New System.Drawing.Point(155, 123)
        Me.lblMosMatri.Name = "lblMosMatri"
        Me.lblMosMatri.Size = New System.Drawing.Size(0, 15)
        Me.lblMosMatri.TabIndex = 12
        '
        'lblMosGrado
        '
        Me.lblMosGrado.AutoSize = True
        Me.lblMosGrado.Location = New System.Drawing.Point(155, 149)
        Me.lblMosGrado.Name = "lblMosGrado"
        Me.lblMosGrado.Size = New System.Drawing.Size(0, 15)
        Me.lblMosGrado.TabIndex = 13
        '
        'lblMosAño
        '
        Me.lblMosAño.AutoSize = True
        Me.lblMosAño.Location = New System.Drawing.Point(155, 174)
        Me.lblMosAño.Name = "lblMosAño"
        Me.lblMosAño.Size = New System.Drawing.Size(0, 15)
        Me.lblMosAño.TabIndex = 14
        '
        'btnBorrar
        '
        Me.btnBorrar.Location = New System.Drawing.Point(159, 204)
        Me.btnBorrar.Name = "btnBorrar"
        Me.btnBorrar.Size = New System.Drawing.Size(190, 27)
        Me.btnBorrar.TabIndex = 15
        Me.btnBorrar.Text = "Borrar alumno"
        Me.btnBorrar.UseVisualStyleBackColor = True
        Me.btnBorrar.Visible = False
        '
        'Formulario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(495, 302)
        Me.Controls.Add(Me.btnBorrar)
        Me.Controls.Add(Me.lblMosAño)
        Me.Controls.Add(Me.lblMosGrado)
        Me.Controls.Add(Me.lblMosMatri)
        Me.Controls.Add(Me.lblMosDNI)
        Me.Controls.Add(Me.lblMosNombre)
        Me.Controls.Add(Me.btnAlumnosCurso)
        Me.Controls.Add(Me.btnAñadir)
        Me.Controls.Add(Me.lblAño)
        Me.Controls.Add(Me.lblGrado)
        Me.Controls.Add(Me.lblMatri)
        Me.Controls.Add(Me.lblDNI)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.btnBuscarMatri)
        Me.Controls.Add(Me.btnBuscarDNI)
        Me.Font = New System.Drawing.Font("Comic Sans MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "Formulario"
        Me.Text = "Practica Datos Aitor, Unai y Toni"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnBuscarDNI As Button
    Friend WithEvents btnBuscarMatri As Button
    Friend WithEvents lblNombre As Label
    Friend WithEvents lblDNI As Label
    Friend WithEvents lblMatri As Label
    Friend WithEvents lblGrado As Label
    Friend WithEvents lblAño As Label
    Friend WithEvents btnAñadir As Button
    Friend WithEvents btnAlumnosCurso As Button
    Friend WithEvents lblMosNombre As Label
    Friend WithEvents lblMosDNI As Label
    Friend WithEvents lblMosMatri As Label
    Friend WithEvents lblMosGrado As Label
    Friend WithEvents lblMosAño As Label
    Friend WithEvents btnBorrar As Button
End Class
