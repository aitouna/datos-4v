﻿Imports Capa_Entidades
Imports Capa_Negocio

Public Class Formulario
    Dim negocio As New Gestion
    Private Sub btnBuscarDNI_Click(sender As Object, e As EventArgs) Handles btnBuscarDNI.Click
        Dim DNI As String
        Dim elAlumno As Alumno


        Do
            DNI = InputBox("Introduzca el DNI", "DNI")
        Loop Until DNI.Length = 9 AndAlso Not IsNumeric(DNI.ElementAt(8))

        elAlumno = negocio.ConsultarAlumnoPorNumeroDNI(DNI)

        If elAlumno.NombreAlumno = "Error" Then
            MessageBox.Show("Error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            lblMosNombre.Text = ""
            lblMosDNI.Text = ""
            lblMosMatri.Text = ""
            lblMosGrado.Text = ""
            lblMosAño.Text = ""
        Else
            lblMosNombre.Text = elAlumno.NombreAlumno
            lblMosDNI.Text = elAlumno.DNI
            lblMosMatri.Text = elAlumno.NumeroMatricula
            lblMosGrado.Text = elAlumno.Grado
            lblMosAño.Text = elAlumno.Año
            btnBorrar.Visible = True
        End If

    End Sub

    Private Sub btnBuscarMatri_Click(sender As Object, e As EventArgs) Handles btnBuscarMatri.Click
        Dim matricula As String
        Dim elAlumno As Alumno
        Do
            matricula = InputBox("Introduzca la matricula", "Matricula")
        Loop Until IsNumeric(matricula)

        elAlumno = negocio.ConsultarAlumnoPorNumeroMatricula(matricula)

        If elAlumno.NombreAlumno = "Error" Then
            MessageBox.Show("Error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            lblMosNombre.Text = ""
            lblMosDNI.Text = ""
            lblMosMatri.Text = ""
            lblMosGrado.Text = ""
            lblMosAño.Text = ""
        Else
            lblMosNombre.Text = elAlumno.NombreAlumno
            lblMosDNI.Text = elAlumno.DNI
            lblMosMatri.Text = elAlumno.NumeroMatricula
            lblMosGrado.Text = elAlumno.Grado
            lblMosAño.Text = elAlumno.Año
            btnBorrar.Visible = True
        End If


    End Sub

    Private Sub btnBorrar_Click(sender As Object, e As EventArgs) Handles btnBorrar.Click
        Dim DNI As String
        Dim confirmacion As String

        DNI = lblMosDNI.Text

        confirmacion = negocio.EliminarAlumno(DNI)

        If confirmacion = "OK" Then
            MessageBox.Show("Alumno borrado", "Borrado", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBox.Show(confirmacion, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
        btnBorrar.Visible = False
        lblMosNombre.Text = ""
        lblMosDNI.Text = ""
        lblMosMatri.Text = ""
        lblMosGrado.Text = ""
        lblMosAño.Text = ""
    End Sub

    Private Sub btnAñadir_Click(sender As Object, e As EventArgs) Handles btnAñadir.Click
        Dim elAlumno As New Alumno
        Dim dato As String
        Dim confirmacion As String

        dato = InputBox("Introduzca el nombre", "Nombre")
        If dato = "" Then
            Exit Sub
        End If
        elAlumno.NombreAlumno = dato

        Do
            dato = InputBox("Introduzca el DNI", "DNI")
            If dato = "" Then
                Exit Sub
            End If
        Loop Until dato.Length = 9 AndAlso Not IsNumeric(dato.ElementAt(8))
        elAlumno.DNI = dato

        Do
            dato = InputBox("Introduzca la matricula", "Matricula")
            If dato = "" Then
                Exit Sub
            End If
        Loop Until IsNumeric(dato)
        elAlumno.NumeroMatricula = Integer.Parse(dato)


        dato = InputBox("Introduzca el grado", "Grado")
        If dato = "" Then
            Exit Sub
        End If
        elAlumno.Grado = dato

        Do
            dato = InputBox("Introduzca el año", "Año")
            If dato = "" Then
                Exit Sub
            End If
        Loop Until IsNumeric(dato) AndAlso dato <= DateTime.Today.year
        elAlumno.Año = Integer.Parse(dato)

        confirmacion = negocio.AñadirAlumno(elAlumno)

        If confirmacion = "OK" Then
            MessageBox.Show("Alumno introducido", "Introducido", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBox.Show(confirmacion, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If


    End Sub

    Private Sub btnAlumnosCurso_Click(sender As Object, e As EventArgs) Handles btnAlumnosCurso.Click
        Dim curso As String
        Dim losAlumnos As String

        curso = InputBox("Introduzca el Curso", "curso")

        losAlumnos = negocio.ConsultarAlumnosPorCurso(curso)

        MessageBox.Show(losAlumnos, "Lista de alumnos", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub


End Class
