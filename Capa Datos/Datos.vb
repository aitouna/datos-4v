﻿Imports Capa_Entidades

Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary

Public Class Datos

    Private Sub CrearDirectorio()

        If Not Directory.Exists(My.Application.Info.DirectoryPath & "\Datos\") Then
            Directory.CreateDirectory(My.Application.Info.DirectoryPath & "\Datos\")
        End If

    End Sub

    Public Function CargarFicheroAlumno() As Dictionary(Of String, Alumno)

        CrearDirectorio()

        Dim diccionario As New Dictionary(Of String, Alumno)

        Try
            Dim fichero As New FileStream("Datos\Clientes.txt", FileMode.Open)
            Dim binary As New BinaryFormatter()
            diccionario = CType(binary.Deserialize(fichero), Dictionary(Of String, Alumno))
            fichero.Close()
        Catch ex As Exception

        End Try

        Return diccionario

    End Function

    Public Function CargarFicheroCurso() As Dictionary(Of String, Curso)

        CrearDirectorio()

        Dim diccionario As New Dictionary(Of String, Curso)

        Try
            Dim fichero As New FileStream("Datos\Pedidos.txt", FileMode.Open)
            Dim binary As New BinaryFormatter()
            diccionario = CType(binary.Deserialize(fichero), Dictionary(Of String, Curso))
            fichero.Close()
        Catch ex As Exception

        End Try

        Return diccionario

    End Function

    Public Sub GrabarFicheroAlumno(ByVal diccionario As Dictionary(Of String, Alumno))

        Try
            Dim fichero As New FileStream(My.Application.Info.DirectoryPath & "\Datos\Clientes.txt", FileMode.Create)
            Dim binary As New BinaryFormatter()
            binary.Serialize(fichero, diccionario)
            fichero.Close()
        Catch ex As Exception
        End Try

    End Sub

    Public Sub GrabarFicheroCurso(ByVal diccionario As Dictionary(Of String, Curso))

        Try
            Dim fichero As New FileStream(My.Application.Info.DirectoryPath & "\Datos\Pedidos.txt", FileMode.Create)
            Dim binary As New BinaryFormatter()
            binary.Serialize(fichero, diccionario)
            fichero.Close()
        Catch ex As Exception

        End Try

    End Sub

End Class
