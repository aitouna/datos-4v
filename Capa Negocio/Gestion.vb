﻿Imports Capa_Entidades
Imports Capa_Datos

Public Class Gestion
    Public Function AñadirAlumno(alumno As Alumno) As String

        Try
            Dim miDatos As New Datos
            Dim miDiccionarioAlumnos As New Dictionary(Of String, Alumno)
            miDiccionarioAlumnos = miDatos.CargarFicheroAlumno()
            If miDiccionarioAlumnos.ContainsKey(alumno.DNI) Then

                'diccionarioError.Add("Error: el alumno ya existe", alumno) 'COMPROBAR
                Return "Error: el alumno ya existe"
            Else
                miDiccionarioAlumnos.Add(alumno.DNI, alumno)
            miDatos.GrabarFicheroAlumno(miDiccionarioAlumnos)
            Return "OK"
            End If

            '  For i As Integer = 0 To miDiccionarioAlumnos.Keys.Count - 1
            ' If miDiccionarioAlumnos(miDiccionarioAlumnos.Keys(i)).DNI = alumno.DNI Then
            'Return "ya existe"
            ' End If
            ' Next
            'miDiccionarioAlumnos.Add(alumno.DNI, alumno)
            'miDatos.GrabarFicheroAlumno(miDiccionarioAlumnos)
            ' Return "OK"

        Catch ex As Exception

            Return "Error: " & ex.ToString

        End Try
    End Function

    Public Function EliminarAlumno(alumno As String)
        Try
            Dim miDatos As New Datos
            Dim miDiccionarioAlumnos As New Dictionary(Of String, Alumno)

            miDiccionarioAlumnos = miDatos.CargarFicheroAlumno
            If miDiccionarioAlumnos.ContainsKey(alumno) Then

                miDiccionarioAlumnos.Remove(alumno)
                miDatos.GrabarFicheroAlumno(miDiccionarioAlumnos)
                Return "OK"

            Else
                Return "Error: el alumno no existe"



            End If
        Catch ex As Exception
            Return "Error inesperado"
        End Try
    End Function



    Public Function ConsultarAlumnosPorCurso(curso As String) As String
        Try
            Dim miDatos As New Datos
            Dim diccionarioAlumnos As New Dictionary(Of String, Alumno)
            diccionarioAlumnos = miDatos.CargarFicheroAlumno
            Dim resultado As String = ""

            For Each alum In diccionarioAlumnos
                If alum.Value.Grado = curso Then
                    resultado = resultado + " " + alum.Value.NombreAlumno
                End If
            Next

            If resultado = "" Then
                resultado = "No existe"
            Else
                Return "Los alumnos son: " + resultado
            End If


        Catch ex As Exception
            Return "Error: " & ex.ToString
        End Try

    End Function

    Public Function ConsultarAlumnoPorNumeroDNI(dni As String) As Alumno

        Dim miDatos As New Datos
        Dim diccionarioAlumnos As New Dictionary(Of String, Alumno)
        diccionarioAlumnos = miDatos.CargarFicheroAlumno
        Dim alumno As New Alumno
        Try
            If diccionarioAlumnos.ContainsKey(dni) Then
                For Each alum In diccionarioAlumnos
                    If alum.Key = dni Then

                        alumno.NombreAlumno = alum.Value.NombreAlumno
                        alumno.Año = alum.Value.Año
                        alumno.DNI = alum.Value.DNI
                        alumno.Grado = alum.Value.Grado
                        alumno.NumeroMatricula = alum.Value.NumeroMatricula
                        Return alumno
                    End If
                Next
            Else
                alumno.NombreAlumno = "Error"
                Return alumno
            End If
        Catch ex As Exception
            alumno.NombreAlumno = "Error"
            Return alumno
        End Try

    End Function

    Public Function ConsultarAlumnoPorNumeroMatricula(matricula As String) As Alumno
        Dim miDatos As New Datos
        Dim miAlumno As New Alumno
        Dim diccionarioAlumnos As New Dictionary(Of String, Alumno)
        diccionarioAlumnos = miDatos.CargarFicheroAlumno
        Try
            For Each alum In diccionarioAlumnos
                If alum.Value.NumeroMatricula = matricula Then
                    miAlumno.Año = alum.Value.Año
                    miAlumno.DNI = alum.Value.DNI
                    miAlumno.Grado = alum.Value.Grado
                    miAlumno.NombreAlumno = alum.Value.NombreAlumno
                    miAlumno.NumeroMatricula = alum.Value.NumeroMatricula
                    Return miAlumno

                End If
            Next
            miAlumno.NombreAlumno = "Error"
            Return miAlumno

        Catch ex As Exception
            miAlumno.NombreAlumno = "Error"
            Return miAlumno
        End Try
    End Function
End Class
