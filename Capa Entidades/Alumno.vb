﻿<Serializable()> Public Class Alumno

    Private _Año As Integer
    Private _NumeroMatricula As Integer
    Private _DNI As String
    Private _Grado As Integer
    Private _NombreAlumno As String



    Public Property Año As Integer
        Get
            Return _Año
        End Get
        Set(value As Integer)
            _Año = value
        End Set
    End Property

    Public Property DNI As String
        Get
            Return _DNI
        End Get
        Set(value As String)
            _DNI = value
        End Set
    End Property

    Public Property Grado As String
        Get
            Return _Grado
        End Get
        Set(value As String)
            _Grado = value
        End Set
    End Property

    Public Property NombreAlumno As String
        Get
            Return _NombreAlumno
        End Get
        Set(value As String)
            _NombreAlumno = value
        End Set
    End Property

    Public Property NumeroMatricula As Integer
        Get
            Return _NumeroMatricula
        End Get
        Set(value As Integer)
            _NumeroMatricula = value
        End Set
    End Property
End Class
