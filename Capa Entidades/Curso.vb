﻿<Serializable()> Public Class Curso

    Private _IdCurso As Integer
    Private _NombreCurso As String


    Public Property IdCurso As Integer
        Get
            Return _IdCurso
        End Get
        Set(value As Integer)
            _IdCurso = value
        End Set
    End Property

    Public Property NombreCurso As String
        Get
            Return _NombreCurso
        End Get
        Set(value As String)
            _NombreCurso = value
        End Set
    End Property
End Class
